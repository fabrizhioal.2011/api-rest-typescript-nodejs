const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Articles = mongoose.model(
	"Articles",
	new Schema({
		created_at: String,
		title: { type: String, default: null },
		url: { type: String, default: null },
		author: String,
		points: { type: Number, default: null },
		story_text: { type: String, default: null },
		comment_text: { type: String, default: null },
		num_comments: { type: Number, default: null },
		story_id: Number,
		story_title: String,
		story_url: String,
		parent_id: Number,
		created_at_i: Number,
		_tags: Array,
		objectID: String,
		_highlightResult: Object,
	})
);

module.exports = Articles;
