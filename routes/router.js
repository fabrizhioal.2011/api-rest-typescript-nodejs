const express = require("express");
const router = express.Router();
const Articles = require("../models/model");

//generalize router get and delete with GET params

router.get("/", (req, res) => {
	let search = {};
	if (req.query.t) {
		//create regExp for search parcial story title results
		let regex =
			"^(?=.*\\b" + req.query.t.replaceAll(" ", "\\b)(?=.*\\b") + "\\b)";
		search = { ...search, story_title: { $regex: regex, $options: "is" } };
	} else if (req.query.a) {
		// define search for author
		search = { ...search, author: req.query.a };
	} else if (req.query.tag) {
		// define search for tags
		search = { ...search, _tags: req.query.tag };
	}

	//execute search
	Articles.find(search)
		.limit(5)
		.exec()
		.then((x) => res.send(x));
});

router.delete("/", (req, res) => {
	// find and delete with id
	Articles.findOneAndDelete({ _id: req.query.d })
		.exec()
		.then(() => res.sendStatus(204));
});

module.exports = router;
