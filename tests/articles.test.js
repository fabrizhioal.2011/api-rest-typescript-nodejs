const supertest = require("supertest");
const { app, server } = require("../server");

const api = supertest(app);

const TITLE = "",
	AUTHOR = "",
	TAG = "",
	ID = "";

//test's
test("return 5 first result", async () => {
	await api
		.get("/")
		.expect(200)
		.expect("Content-Type", /application\/json/);
});

test("return 5 first with a author name", async () => {
	await api
		.get(`/?a=${AUTHOR}`)
		.expect(200)
		.expect("Content-Type", /application\/json/);
});

test("return 5 first with a tag", async () => {
	await api
		.get(`/?tag=${TAG}`)
		.expect(200)
		.expect("Content-Type", /application\/json/);
});

test("return 5 first with story title parcial", async () => {
	await api
		.get(`/?t=${TITLE}`)
		.expect(200)
		.expect("Content-Type", /application\/json/);
});

test("Find and delete article with ID", async () => {
	await api.delete(`/?d=${ID}`).expect(204);
});

afterAll(() => server.close());
