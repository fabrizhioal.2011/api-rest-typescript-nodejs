# api-rest-nodejs

Created with Fabrizhio Al chariti

Technologies:

- JavaScript
- MongoDB (mongoose)
- ExpressJS
- Node-Fetch

## Getting started

1. open bash or command line in server folder

2. Run command "npm -i"

3. Open server.js

4. Modify URI_CONNECT_MONGODB ~ before creation of database

5. Run in bash or command line "npm run start"

## How to use

Para obtener los primeros 5:

Entrar en http://localhost:3000 or default URL

Para obtener los primeros 5 resultados definiendo un autor:

Entrar en http://localhost:3000?a="AUTHOR HERE"

Para obtener los primeros 5 resultados definiendo un tag:

Entrar en http://localhost:3000?tag="TAG HERE"

Para obtener los primeros 5 resultados definiendo un tag:

Entrar en http://localhost:3000?t="TITLE HERE"

Para eliminar un resultado de la base de datos:

Entrar en http://localhost:3000?d="ID HERE"

## How to test

Modify ID in test/articles.test.js for delete any article

Modify TITLE in test/articles.test.js for search article's with title parcial

Modify AUTHOR in test/articles.test.js for search article's with author name

Modify TAG in test/articles.test.js for search article's with tag
