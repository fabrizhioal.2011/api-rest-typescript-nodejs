const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
// call to routes
const router = require("./routes/router");

// call to controllers
const update = require("./controllers/updateData");

const app = express();

// //Load new data per hour
// update();
// setInterval(() => update(), 3600000);

// single connection to mongodb
const uri =
	"mongodb://fabrizhioda:fabri12@atw-db-shard-00-00.ta0oe.mongodb.net:27017,atw-db-shard-00-01.ta0oe.mongodb.net:27017,atw-db-shard-00-02.ta0oe.mongodb.net:27017/serverapitest?ssl=true&replicaSet=atlas-zlf1mg-shard-0&authSource=admin&retryWrites=true&w=majority";

mongoose.connect(uri, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

//Uses for the app

//Parse of body to read data
app.use(bodyParser.json());

app.use(cors());

app.use("/", router);

//

const server = app.listen(3000, () => {
	console.log("server active");
});

module.exports = { app, server };
