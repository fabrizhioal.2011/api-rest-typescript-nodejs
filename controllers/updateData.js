//require external module "node fetch" to fetch data from another API
const fetch = require("node-fetch");

//require model mongodb document for articles
const Article = require("../models/model");

//create a function arrow to get data from another api, delete current data and insert new data
const updateDataPerHour = async () => {
	await Article.deleteMany({}).then(() =>
		console.log("all old data was successfully deleted")
	);
	let data;
	await fetch("https://hn.algolia.com/api/v1/search_by_date?query=nodejs")
		.then((promesa) => promesa.json())
		.then((contenido) => (data = contenido.hits));
	data.map((x) => {
		Article.create({ ...x }).then(() => console.log("successfully inserted"));
	});
};

module.exports = updateDataPerHour;
